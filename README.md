# annexes-rits-2024

Ce dépôt est une annexe pour la soumission à la conférence RITS 2024 du papier suivant : Méthodes de définition de la vérité terrain pour les systèmes par apprentissage d'aide à la décision clinique.

## Auteurs
- Alex Poiron (1)
- Sandie Cabon (1)
- Marc Cuggia (1)

(1) Univ Rennes, CHU Rennes, INSERM, LTSI-UMR 1099, Rennes, France

## Documents
Vous trouverez dans ce dépôt un document listant l'ensemble des articles retenus pour la revue de la littérature.
